CC=gcc
CFLAGS=-Wall -Wextra -std=c99 \
       -pipe -march=native -O2 \
       -D_FORTIFY_SOURCE=2 \
       -fstack-protector-strong \
       -fcf-protection 
LIBS=
PREFIX=/usr/local/bin

OBJQ = replace.o main.o

%.o: src/%.c
	@echo CC $^
	@$(CC) -c -o $@ $< $(CFLAGS)

replace: $(OBJQ)
	@echo CC $^
	@$(CC) -o $@ $^ $(CLFAGS) $(LIBS)
	@rm -f *.o

clean:
	@echo Removing binaries
	@rm -f replace *.o

install:
	@echo INSTALL replace
	@install -m755 replace $(PREFIX)/replace
