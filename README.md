# replace

This is a tool inspired by the replace program that is part of mariadb-server.

It is however more simple than the one from mariadb, as it only supports literal
matches (that is the bytes are the same) and not regex matches etc. (which
mariadb's replace provides).

## Build

Run `make && sudo make install`
