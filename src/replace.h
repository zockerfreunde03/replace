// SPDX-FileCopyrightText: 2022 zockerfreunde03 <zocker@10zen.eu>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef REPLACE_H
#define REPLACE_H

#include <stdlib.h>

#define NOT_FOUND 0xAA
#define TOO_LARGE 0xBB
#define IDENTICAL 0xCC

size_t find_substr(char *, char *);
size_t _find_substr(char *, char *, int);
int count_substr(char *, char *);

char *replace_once(char *, char *, char *);
char *replace_all(char *, char *, char *);

void *_realloc(void *, int, int);
void *switch_buf(void *, void *);
#endif
