// SPDX-FileCopyrightText: 2022 zockerfreunde03 <zocker@10zen.eu>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdlib.h>
#include <string.h>
#include "replace.h"

// Search `orig` for `str` and report the index where it occurs,
// if found. If not return -NOT_FOUND.
size_t find_substr(char *orig, char *str)
{
	if (strlen(orig) < strlen(str))
		return -TOO_LARGE;

	if (!strcmp(orig, str))
		return -IDENTICAL;

	size_t offset = 0;
	while (!_find_substr(orig, str, offset)) {
		++offset;
		if (offset >= strlen(orig))
			return -NOT_FOUND;
	}

	return offset;
}

// Search offset + strlen(str) for the bytes that
// make up `str` in `orig` and return 1 if we found
// all of them in the right orderand 0 otherwise
size_t _find_substr(char *orig, char *str, int offset)
{
	int cnt = 0;
	int size = strlen(str);
	char *tmp = calloc(size + 1, sizeof(char));
	for (int i = 0; i < size; ++i) {
		if (orig[offset + i] == str[i]) {
			tmp[i] = orig[offset + i];
			++cnt;
		}
	}

	size_t ret = cnt == size && !strcmp(tmp, str);
	free(tmp);
	return ret;
}

// Count the number of `str` in `orig`
// If it does (or can) not contain any it returns 0
int count_substr(char *orig, char *str)
{
	size_t size = strlen(orig);
	if (size < strlen(str))
		return 0;

	if (!strcmp(orig, str))
		return 1;

	int cnt = 0;

	for (size_t i = 0; i < size; ++i) {
		if (_find_substr(orig, str, i) == 1)
			++cnt;
	}

	return cnt;
}

// Find the pattern substring (if it exists) and replace
// it with `sub`. Return NULL if `pattern` is not found,
// larger than `orig` or identical to `orig`
char *replace_once(char *orig, char *pattern, char *sub)
{
	int offset = find_substr(orig, pattern);

	if (offset == -NOT_FOUND)
		return NULL;

	if (offset == -TOO_LARGE)
		return NULL;

	if (offset == -IDENTICAL)
		return NULL;

	size_t len_orig = strlen(orig);
	size_t len_pattern = strlen(pattern);
	size_t len_sub = strlen(sub);

	size_t size = len_orig + (len_sub - len_pattern);

	char *buf = calloc(size + 1, sizeof(char));

	size_t len_rest = len_orig - len_pattern;

	strncat(buf, orig, offset);
	strncat(buf, sub, len_sub);
	strncat(buf, orig + offset + len_pattern, len_rest);

	return buf;
}

// Call replace_once until replace_once returns NULL
char *replace_all(char *orig, char *pattern, char *sub)
{
	// Test the error cases beforehand
	size_t test = find_substr(orig, pattern);
	switch (test) {
	case -NOT_FOUND:
		return NULL;
	case -IDENTICAL:
		return sub;
	case -TOO_LARGE:
		return NULL;
	default:
		break;
	}

	int max = count_substr(orig, pattern);

	for (int i = 0; i < max; ++i) {
		if (i)
			orig = switch_buf(orig,
					  replace_once(orig, pattern, sub));
		else
			orig = replace_once(orig, pattern, sub);
	}

	return orig;
}

// Simple hack to rellocate cleared memory
void *_realloc(void *buf, int nmeb, int size)
{
	free(buf);
	return calloc(nmeb, size);
}

// Simple hack to switch buffers w/o mem leak
void *switch_buf(void *old, void *new)
{
	free(old);
	return new;
}
