// SPDX-FileCopyrightText: 2022 zockerfreunde03 <zocker@10zen.eu>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "replace.h"

#define print_usage(x) fprintf(stderr, "%s FROM TO [--replace-once]\n", x)

void parse(int, char **);
char *remap_specials(char *);

static int REPLACE_ONCE = 0;

int main(int argc, char **argv)
{
	parse(argc, argv);

	if (argc < 3) {
		print_usage(argv[0]);
		return EXIT_FAILURE;
	}

	char *pattern = remap_specials(argv[1]);
	char *substitute = remap_specials(argv[2]);

	size_t size = 1024 * 4;

	char *in = calloc(size + 1, sizeof(char));

	if (!in) {
		perror("replace:");
		free(in);
		return EXIT_FAILURE;
	}

	char *ret;
	while (read(STDIN_FILENO, in, size) > 0) {
		if (!REPLACE_ONCE)
			ret = replace_all(in, pattern, substitute);
		else
			ret = replace_once(in, pattern, substitute);

		if (ret == NULL) {
			printf("%s", in);
			continue;
		}

		if (!strcmp(ret, in))
			printf("%s", substitute);
		else
			printf("%s", ret);

		free(ret);
	}

	free(in);
	return EXIT_SUCCESS;
}

void parse(int argc, char **argv)
{
	for (int i = 0; i < argc; ++i) {
		if (!strcmp(argv[i], "--replace-once"))
			REPLACE_ONCE = 1;
	}
}

char *remap_specials(char *x)
{
	if (!strcmp(x, "\\n"))
		return "\n";

	if (!strcmp(x, "\\t"))
		return "\t";

	return x;
}
